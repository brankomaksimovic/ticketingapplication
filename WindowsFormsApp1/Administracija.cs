﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace WindowsFormsApp1
{
    public partial class Administracija : MetroForm
    {
        public Administracija()
        {
            InitializeComponent();
            naputiDataGrid();
            popuniCombobox();


        }
        public void naputiDataGrid()
        {
            SqlConnection kon = new SqlConnection(Konekcija.con);
            kon.Open();
            SqlCommand komanda = new SqlCommand("SP_DajSveTikete", kon);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(komanda);
            adap.Fill(tabela);
            dataGridView1.DataSource = tabela;
            kon.Close();


        }
        public void popuniCombobox()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("DajSveVlasnike", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "ime_i_prezime";
            comboBox1.ValueMember = "IDvlasnik";
            konekcija.Close();
        }

        public void PopuniGridPoVlasniku(int idvlasnik)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("DajTiketePoVlasniku", konekcija);
            komanda.Parameters.AddWithValue("@idvlasnik", idvlasnik);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            dataGridView1.DataSource = tabela;
            konekcija.Close();

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PopuniGridPoVlasniku((int)comboBox1.SelectedValue);
        }
        public void obrisiTiket()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("BrisanjeTiketa", konekcija);
            komanda.Parameters.AddWithValue("@idticket", dataGridView1.CurrentRow.Cells[0].Value);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.ExecuteNonQuery();
            konekcija.Close();
            PopuniGridPoVlasniku((int)comboBox1.SelectedValue);
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            obrisiTiket();
           
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            NoviTiket nt = new NoviTiket();
            nt.ShowDialog();

        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            IzmenaTiketa it = new IzmenaTiketa((int)dataGridView1.CurrentRow.Cells[0].Value);
            it.ShowDialog();

        }
        public void PrebaciTiketNaOtvoren()
        {

            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("PrebaciTiketNaOtvoren", konekcija);
            komanda.Parameters.AddWithValue("@idtiket", dataGridView1.CurrentRow.Cells[0].Value);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.ExecuteNonQuery();
            konekcija.Close();
            PopuniGridPoVlasniku((int)comboBox1.SelectedValue);
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            PrebaciTiketNaOtvoren();

        }
        public void PrebaciTiketNaZatvoren()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("PrebaciTiketNaZatvoren", konekcija);
            komanda.Parameters.AddWithValue("@idtiket", dataGridView1.CurrentRow.Cells[0].Value);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.ExecuteNonQuery();
            konekcija.Close();
            PopuniGridPoVlasniku((int)comboBox1.SelectedValue);


        }
        public void PrebaciTiketNaZakljucan()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("PrebaciTiketNaZakljucan", konekcija);
            komanda.Parameters.AddWithValue("@tiket", dataGridView1.CurrentRow.Cells[0].Value);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.ExecuteNonQuery();
            konekcija.Close();
            PopuniGridPoVlasniku((int)comboBox1.SelectedValue);

        }

        private void metroButton5_Click(object sender, EventArgs e)
        {
            PrebaciTiketNaZatvoren();


        }

        private void metroButton6_Click(object sender, EventArgs e)
        {
            PrebaciTiketNaZakljucan();

        }

    }
}
