﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace WindowsFormsApp1
{
    public partial class BrisanjeTiketa :  MetroForm
    {
        public BrisanjeTiketa()
        {
            InitializeComponent();
            popuniDataGrid();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void popuniDataGrid()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("SP_DajSveTikete",konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            dataGridView1.DataSource = tabela;


        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("BrisanjeTiketa", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            int id = Convert.ToInt32( dataGridView1.CurrentRow.Cells[0].Value.ToString());
            komanda.Parameters.AddWithValue("@idticket", id);
            komanda.ExecuteNonQuery();
            konekcija.Close();
            popuniDataGrid();

        }

    }
}
