﻿namespace WindowsFormsApp1
{
    partial class GlavniMeni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.visualGauge1 = new VisualPlus.Toolkit.Controls.DataVisualization.VisualGauge();
            this.visualGauge2 = new VisualPlus.Toolkit.Controls.DataVisualization.VisualGauge();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.visualButton1 = new VisualPlus.Toolkit.Controls.Interactivity.VisualButton();
            this.visualButton2 = new VisualPlus.Toolkit.Controls.Interactivity.VisualButton();
            this.visualButton3 = new VisualPlus.Toolkit.Controls.Interactivity.VisualButton();
            this.visualButton4 = new VisualPlus.Toolkit.Controls.Interactivity.VisualButton();
            this.visualButton5 = new VisualPlus.Toolkit.Controls.Interactivity.VisualButton();
            this.visualButton6 = new VisualPlus.Toolkit.Controls.Interactivity.VisualButton();
            this.visualButton7 = new VisualPlus.Toolkit.Controls.Interactivity.VisualButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 76);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(498, 323);
            this.dataGridView1.TabIndex = 0;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(296, 46);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(81, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Statusi tiketa";
            // 
            // visualGauge1
            // 
            this.visualGauge1.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.visualGauge1.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.visualGauge1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.visualGauge1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge1.LargeChange = 5;
            this.visualGauge1.Location = new System.Drawing.Point(594, 76);
            this.visualGauge1.Margin = new System.Windows.Forms.Padding(6);
            this.visualGauge1.Maximum = 100;
            this.visualGauge1.MaximumVisible = true;
            this.visualGauge1.Minimum = 0;
            this.visualGauge1.MinimumVisible = true;
            this.visualGauge1.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualGauge1.Name = "visualGauge1";
            this.visualGauge1.Progress = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(136)))), ((int)(((byte)(45)))));
            this.visualGauge1.ProgressVisible = true;
            this.visualGauge1.Size = new System.Drawing.Size(174, 117);
            this.visualGauge1.SmallChange = 1;
            this.visualGauge1.TabIndex = 3;
            this.visualGauge1.Text = "visualGauge1";
            this.visualGauge1.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualGauge1.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge1.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge1.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge1.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualGauge1.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualGauge1.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualGauge1.Thickness = 25;
            // 
            // visualGauge2
            // 
            this.visualGauge2.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.visualGauge2.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.visualGauge2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.visualGauge2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge2.LargeChange = 5;
            this.visualGauge2.Location = new System.Drawing.Point(608, 268);
            this.visualGauge2.Margin = new System.Windows.Forms.Padding(6);
            this.visualGauge2.Maximum = 100;
            this.visualGauge2.MaximumVisible = true;
            this.visualGauge2.Minimum = 0;
            this.visualGauge2.MinimumVisible = true;
            this.visualGauge2.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualGauge2.Name = "visualGauge2";
            this.visualGauge2.Progress = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(136)))), ((int)(((byte)(45)))));
            this.visualGauge2.ProgressVisible = true;
            this.visualGauge2.Size = new System.Drawing.Size(174, 117);
            this.visualGauge2.SmallChange = 1;
            this.visualGauge2.TabIndex = 4;
            this.visualGauge2.Text = "visualGauge2";
            this.visualGauge2.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualGauge2.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge2.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge2.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualGauge2.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualGauge2.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualGauge2.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualGauge2.Thickness = 25;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(624, 60);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(102, 19);
            this.metroLabel2.TabIndex = 5;
            this.metroLabel2.Text = "Otvorenih tiketa";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(643, 240);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(106, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Zatvorenih tiketa";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(383, 44);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // visualButton1
            // 
            this.visualButton1.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton1.BackColorState.Enabled = System.Drawing.Color.SteelBlue;
            this.visualButton1.BackColorState.Hover = System.Drawing.Color.Aqua;
            this.visualButton1.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.visualButton1.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visualButton1.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.visualButton1.Border.HoverVisible = true;
            this.visualButton1.Border.Rounding = 6;
            this.visualButton1.Border.Thickness = 1;
            this.visualButton1.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.visualButton1.Border.Visible = true;
            this.visualButton1.ForeColor = System.Drawing.Color.White;
            this.visualButton1.Image = null;
            this.visualButton1.Location = new System.Drawing.Point(6, 20);
            this.visualButton1.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualButton1.Name = "visualButton1";
            this.visualButton1.Size = new System.Drawing.Size(82, 20);
            this.visualButton1.TabIndex = 8;
            this.visualButton1.Text = "Novi tiket";
            this.visualButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.visualButton1.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualButton1.TextStyle.Enabled = System.Drawing.Color.White;
            this.visualButton1.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton1.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton1.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton1.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton1.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualButton1.Click += new System.EventHandler(this.visualButton1_Click);
            // 
            // visualButton2
            // 
            this.visualButton2.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton2.BackColorState.Enabled = System.Drawing.Color.SteelBlue;
            this.visualButton2.BackColorState.Hover = System.Drawing.Color.Aqua;
            this.visualButton2.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.visualButton2.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visualButton2.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.visualButton2.Border.HoverVisible = true;
            this.visualButton2.Border.Rounding = 6;
            this.visualButton2.Border.Thickness = 1;
            this.visualButton2.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.visualButton2.Border.Visible = true;
            this.visualButton2.ForeColor = System.Drawing.Color.White;
            this.visualButton2.Image = null;
            this.visualButton2.Location = new System.Drawing.Point(94, 20);
            this.visualButton2.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualButton2.Name = "visualButton2";
            this.visualButton2.Size = new System.Drawing.Size(82, 20);
            this.visualButton2.TabIndex = 9;
            this.visualButton2.Text = "Izmena tiketa";
            this.visualButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.visualButton2.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualButton2.TextStyle.Enabled = System.Drawing.Color.White;
            this.visualButton2.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton2.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton2.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton2.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton2.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualButton2.Click += new System.EventHandler(this.visualButton2_Click);
            // 
            // visualButton3
            // 
            this.visualButton3.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton3.BackColorState.Enabled = System.Drawing.Color.SteelBlue;
            this.visualButton3.BackColorState.Hover = System.Drawing.Color.Aqua;
            this.visualButton3.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.visualButton3.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visualButton3.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.visualButton3.Border.HoverVisible = true;
            this.visualButton3.Border.Rounding = 6;
            this.visualButton3.Border.Thickness = 1;
            this.visualButton3.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.visualButton3.Border.Visible = true;
            this.visualButton3.ForeColor = System.Drawing.Color.White;
            this.visualButton3.Image = null;
            this.visualButton3.Location = new System.Drawing.Point(182, 20);
            this.visualButton3.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualButton3.Name = "visualButton3";
            this.visualButton3.Size = new System.Drawing.Size(82, 20);
            this.visualButton3.TabIndex = 10;
            this.visualButton3.Text = "Brisanje tiketa";
            this.visualButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.visualButton3.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualButton3.TextStyle.Enabled = System.Drawing.Color.White;
            this.visualButton3.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton3.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton3.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton3.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton3.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualButton3.Click += new System.EventHandler(this.visualButton3_Click);
            // 
            // visualButton4
            // 
            this.visualButton4.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton4.BackColorState.Enabled = System.Drawing.Color.LightSkyBlue;
            this.visualButton4.BackColorState.Hover = System.Drawing.Color.Aqua;
            this.visualButton4.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.visualButton4.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visualButton4.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.visualButton4.Border.HoverVisible = true;
            this.visualButton4.Border.Rounding = 6;
            this.visualButton4.Border.Thickness = 1;
            this.visualButton4.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.visualButton4.Border.Visible = true;
            this.visualButton4.ForeColor = System.Drawing.Color.White;
            this.visualButton4.Image = null;
            this.visualButton4.Location = new System.Drawing.Point(6, 46);
            this.visualButton4.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualButton4.Name = "visualButton4";
            this.visualButton4.Size = new System.Drawing.Size(82, 20);
            this.visualButton4.TabIndex = 11;
            this.visualButton4.Text = "Otvoren";
            this.visualButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.visualButton4.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualButton4.TextStyle.Enabled = System.Drawing.Color.White;
            this.visualButton4.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton4.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton4.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton4.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton4.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualButton4.Click += new System.EventHandler(this.visualButton4_Click);
            // 
            // visualButton5
            // 
            this.visualButton5.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton5.BackColorState.Enabled = System.Drawing.Color.LightSkyBlue;
            this.visualButton5.BackColorState.Hover = System.Drawing.Color.Aqua;
            this.visualButton5.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.visualButton5.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visualButton5.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.visualButton5.Border.HoverVisible = true;
            this.visualButton5.Border.Rounding = 6;
            this.visualButton5.Border.Thickness = 1;
            this.visualButton5.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.visualButton5.Border.Visible = true;
            this.visualButton5.ForeColor = System.Drawing.Color.White;
            this.visualButton5.Image = null;
            this.visualButton5.Location = new System.Drawing.Point(94, 46);
            this.visualButton5.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualButton5.Name = "visualButton5";
            this.visualButton5.Size = new System.Drawing.Size(82, 20);
            this.visualButton5.TabIndex = 12;
            this.visualButton5.Text = "Zatvoren";
            this.visualButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.visualButton5.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualButton5.TextStyle.Enabled = System.Drawing.Color.White;
            this.visualButton5.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton5.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton5.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton5.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton5.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualButton5.Click += new System.EventHandler(this.visualButton5_Click);
            // 
            // visualButton6
            // 
            this.visualButton6.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton6.BackColorState.Enabled = System.Drawing.Color.LightSkyBlue;
            this.visualButton6.BackColorState.Hover = System.Drawing.Color.Aqua;
            this.visualButton6.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.visualButton6.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visualButton6.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.visualButton6.Border.HoverVisible = true;
            this.visualButton6.Border.Rounding = 6;
            this.visualButton6.Border.Thickness = 1;
            this.visualButton6.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.visualButton6.Border.Visible = true;
            this.visualButton6.ForeColor = System.Drawing.Color.White;
            this.visualButton6.Image = null;
            this.visualButton6.Location = new System.Drawing.Point(182, 46);
            this.visualButton6.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualButton6.Name = "visualButton6";
            this.visualButton6.Size = new System.Drawing.Size(82, 20);
            this.visualButton6.TabIndex = 13;
            this.visualButton6.Text = "Zakljucan";
            this.visualButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.visualButton6.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualButton6.TextStyle.Enabled = System.Drawing.Color.White;
            this.visualButton6.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton6.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton6.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton6.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton6.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualButton6.Click += new System.EventHandler(this.visualButton6_Click);
            // 
            // visualButton7
            // 
            this.visualButton7.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton7.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.visualButton7.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.visualButton7.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.visualButton7.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visualButton7.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.visualButton7.Border.HoverVisible = true;
            this.visualButton7.Border.Rounding = 6;
            this.visualButton7.Border.Thickness = 1;
            this.visualButton7.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.visualButton7.Border.Visible = true;
            this.visualButton7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton7.Image = null;
            this.visualButton7.Location = new System.Drawing.Point(641, 20);
            this.visualButton7.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.visualButton7.Name = "visualButton7";
            this.visualButton7.Size = new System.Drawing.Size(85, 20);
            this.visualButton7.TabIndex = 14;
            this.visualButton7.Text = "Osvezi";
            this.visualButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.visualButton7.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.visualButton7.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton7.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton7.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.visualButton7.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton7.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.visualButton7.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.visualButton7.Click += new System.EventHandler(this.visualButton7_Click);
            // 
            // GlavniMeni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 411);
            this.Controls.Add(this.visualButton7);
            this.Controls.Add(this.visualButton6);
            this.Controls.Add(this.visualButton5);
            this.Controls.Add(this.visualButton4);
            this.Controls.Add(this.visualButton3);
            this.Controls.Add(this.visualButton2);
            this.Controls.Add(this.visualButton1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.visualGauge2);
            this.Controls.Add(this.visualGauge1);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "GlavniMeni";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private VisualPlus.Toolkit.Controls.DataVisualization.VisualGauge visualGauge1;
        private VisualPlus.Toolkit.Controls.DataVisualization.VisualGauge visualGauge2;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.ComboBox comboBox1;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualButton visualButton1;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualButton visualButton2;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualButton visualButton3;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualButton visualButton4;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualButton visualButton5;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualButton visualButton6;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualButton visualButton7;
    }
}