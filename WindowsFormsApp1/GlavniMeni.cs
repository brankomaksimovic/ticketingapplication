﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace WindowsFormsApp1
{
    public partial class GlavniMeni : MetroForm
    {
        public GlavniMeni()
        {
            InitializeComponent();
            naputiDataGrid();
            popuniCombobox();
            visualGauge1.Value = izracunajPostotakOtvorenih();
            visualGauge2.Value = izracunajPostotakZatvorenih();
            comboBox1.SelectedValue = 1002;



        }
        public void naputiDataGrid()
        {
            SqlConnection kon = new SqlConnection(Konekcija.con);
            kon.Open();
            SqlCommand komanda = new SqlCommand("SP_DajSveTikete",kon);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(komanda);
            adap.Fill(tabela);
            dataGridView1.DataSource = tabela;
            kon.Close();


        }
        public void napuniDatagridPotiputiketa(string status)
{

            try
            {
                int status1 = Convert.ToInt32(status);
                SqlConnection kon = new SqlConnection(Konekcija.con);
                kon.Open();
                SqlCommand komanda = new SqlCommand("SP_Daj_tikete_po_tipu", kon);
                komanda.CommandType = CommandType.StoredProcedure;
                komanda.Parameters.AddWithValue("@idstatustiketa", status1);
                DataTable tabela = new DataTable();
                SqlDataAdapter adap = new SqlDataAdapter(komanda);
                adap.Fill(tabela);
                dataGridView1.DataSource = tabela;
                kon.Close();
            }catch(Exception e)
            {

            }

        }
         public void popuniCombobox()
        {
           

                SqlConnection konekcija = new SqlConnection(Konekcija.con);
                konekcija.Open();
                SqlCommand komanda = new SqlCommand("Select naziv_statusa,IDstatus from Status_tiketa", konekcija);
                DataTable tabela = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(komanda);
                adapter.Fill(tabela);
                comboBox1.DataSource = tabela;
               comboBox1.DisplayMember = "naziv_statusa";
               comboBox1.ValueMember = "IDstatus";
                konekcija.Close();
            
        }

       
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
             napuniDatagridPotiputiketa(comboBox1.SelectedValue.ToString());
            }
            catch (Exception exx){ }
                
           
        }
        public int izracunajPostotakOtvorenih()
        {
            String rezultat="";
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("IzracunavanjePostotkaOtvorenih", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            foreach(DataRow dr in tabela.Rows)
            {
                rezultat = dr["rezultat"].ToString();
            }
            int rezultat1 = Convert.ToInt32(rezultat);
            return rezultat1;

        }
        public int izracunajPostotakZatvorenih()
        {
            String rezultat = "";
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("IzracunavanjePostotkaZatvorenih", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            foreach(DataRow dr in tabela.Rows)
            {
                rezultat = dr["rezultat"].ToString();
            }
            int rezultat2 = Convert.ToInt32(rezultat);
            return rezultat2;
        }

        private void visualButton1_Click(object sender, EventArgs e)
        {
            NoviTiket nt = new NoviTiket();
            nt.Show();

        }

        private void visualButton4_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("PrebaciTiketNaOtvoren", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            komanda.Parameters.AddWithValue("@idtiket", id);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Uspesno ste izmenili status tiketa na otvoren!");
            OsveziSve();

           

        }

        private void visualButton3_Click(object sender, EventArgs e)
        {
            BrisanjeTiketa bt = new BrisanjeTiketa();
            bt.Show();

        }

        private void visualButton7_Click(object sender, EventArgs e)
        {
            OsveziSve();

        }
        public void OsveziSve()
        {
            visualGauge1.Value = izracunajPostotakOtvorenih();
            visualGauge2.Value = izracunajPostotakZatvorenih();
            naputiDataGrid();
        }


        private void visualButton2_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            IzmenaTiketa iz = new IzmenaTiketa(id);
            iz.Show();

        }

        private void visualButton5_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("PrebaciTiketNaZatvoren", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            komanda.Parameters.AddWithValue("@idtiket", id);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Status tiketa promenjen u zatvoren!");
            OsveziSve();


        }

        private void visualButton6_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("PrebaciTiketNaZakljucan", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            komanda.Parameters.AddWithValue("@tiket", id);
            komanda.ExecuteNonQuery();
            MessageBox.Show("Prebacili ste tiket na zakljucan!");
            OsveziSve();

        }
    }
}
