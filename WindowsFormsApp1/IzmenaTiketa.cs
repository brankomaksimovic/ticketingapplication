﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace WindowsFormsApp1
{
    public partial class IzmenaTiketa : MetroForm
    {
       public int idtiket;

        public IzmenaTiketa(int idtiket)
        {
            InitializeComponent();
            popuniCombobox();
            popuniVlasnike();
            popuniTiptiketa();
            this.idtiket = idtiket;
            popuniInformacije(idtiket);



        }

        private void IzmenaTiketa_Load(object sender, EventArgs e)
        {

        }
        public void popuniCombobox()
        {


            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("Select naziv_statusa,IDstatus from Status_tiketa", konekcija);
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox1.DataSource = tabela;
            comboBox1.DisplayMember = "naziv_statusa";
            comboBox1.ValueMember = "IDstatus";
            konekcija.Close();

        }
        public void popuniVlasnike()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("[DajSveVlasnike]", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox4.DataSource = tabela;
            comboBox4.DisplayMember = "ime_i_prezime";
            comboBox4.ValueMember = "IDvlasnik";
            konekcija.Close();

        }
        public void popuniTiptiketa()
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("DajSveTipoveTiketa", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            DataTable tabela = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(komanda);
            adapter.Fill(tabela);
            comboBox2.DataSource = tabela;
            comboBox2.DisplayMember = "naziv_tipa";
            comboBox2.ValueMember = "IDtip";
            konekcija.Close();

        }
        public void popuniInformacije(int id)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("DajMiTiketPoIDu", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.Parameters.AddWithValue("@idticket",id);
            SqlDataReader rider = komanda.ExecuteReader();
            while (rider.Read())
            {
                textBox1.Text = rider.GetString(1);
                richTextBox1.Text = rider.GetString(2);
                comboBox1.SelectedValue = rider.GetInt32(4);
                comboBox2.SelectedValue = rider.GetInt32(7);
                comboBox4.SelectedValue = rider.GetInt32(6);


            }

        }

        private void visualButton1_Click(object sender, EventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("IzmeniTiket", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.Parameters.AddWithValue("@idticket",idtiket);
            komanda.Parameters.AddWithValue("@naslovtiketa", textBox1.Text);
            komanda.Parameters.AddWithValue("@teksttiketa",richTextBox1.Text);
            komanda.Parameters.AddWithValue("@idstatus",(int) comboBox1.SelectedValue);
            komanda.Parameters.AddWithValue("@idtip",(int)comboBox2.SelectedValue);
            komanda.Parameters.AddWithValue("@idvlasnik",(int) comboBox4.SelectedValue);
            komanda.ExecuteNonQuery();
            konekcija.Close();
            MessageBox.Show("Uspesno ste izmenili tiket!");

        }
    }
}
