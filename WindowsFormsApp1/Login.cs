﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace WindowsFormsApp1
{
    public partial class Login :  MetroForm
    {
        public Login()
        {
            InitializeComponent();
        }

        private void visualTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void visualButton1_Click(object sender, EventArgs e)
        {
            int i = 0;
            SqlConnection konekcija = new SqlConnection(Konekcija.con);
            konekcija.Open();
            SqlCommand komanda = new SqlCommand("LoginProcedura", konekcija);
            komanda.CommandType = CommandType.StoredProcedure;
            komanda.Parameters.AddWithValue("@username", visualTextBox1.Text);
            komanda.Parameters.AddWithValue("@password", visualTextBox2.Text);
            SqlDataReader rd = komanda.ExecuteReader();
            int tipkorisnika = 0;
           
            while (rd.Read())
            {

                i++;
              tipkorisnika =(int)  rd["uloga"];
            }

            if (i != 0) {

                if (tipkorisnika == 1)
                {
                    GlavniMeni gl = new GlavniMeni();
                    gl.ShowDialog();
                }
                else
                {
                    Administracija a = new Administracija();
                    a.ShowDialog();
                }
           
            }
            else
            {
                MessageBox.Show("Neispravan username ili password!");
            }


            // this.Hide();

        }
    }
}
